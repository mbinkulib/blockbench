#!/bin/bash
#arg nnodes
cd `dirname ${BASH_SOURCE-$0}`
. env.sh
. env_main.sh

i=0
for host in `cat $CLIENTS`; do
  if [[ $i -lt $2 ]]; then
    sudo ssh -o StrictHostKeyChecking=no $host sudo killall -KILL driver 
    echo done node $host
  fi
  let i=$i+1
done
i=0
for host in `cat $HOSTS`; do
  if [[ $i -lt $1 ]]; then
    sudo ssh -o StrictHostKeyChecking=no $host $QUO_HOME/stop.sh
    echo done node $host
  fi
  let i=$i+1
done


