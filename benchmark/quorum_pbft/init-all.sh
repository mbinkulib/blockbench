#!/bin/bash
# num_nodes
cd `dirname ${BASH_SOURCE-$0}`
. env.sh
. env_main.sh

echo "init-all"
i=0
rm -rf ips
./gather.sh $1
./modify_ip.sh $1


for host in `cat $HOSTS`; do
  if [[ $i -lt $1 ]]; then
    sudo scp static-nodes/static-nodes$1.json $host:$QUO_HOME/static-nodes/static-nodes$1.json
    sudo scp genesis$1.json $host:$QUO_HOME/genesis$1.json
    sudo ssh -o StrictHostKeyChecking=no $host $QUO_HOME/init.sh $i $1
    echo done node $host
  fi
  let i=$i+1
done
