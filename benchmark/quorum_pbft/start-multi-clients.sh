#!/bin/bash
#num_clients num_nodes threads tx_rate sim_time[-drop]
cd `dirname ${BASH_SOURCE-$0}`
. env.sh
. env_main.sh
let i=0
let IDX=$1/2
echo $IDX
for client in `cat $CLIENTS`; do
  if [[ $i -lt $1 ]]; then
#    scp addPeer.txt $client:$QUO_HOME
    sudo scp clients $client:$QUO_HOME
    sudo scp hosts $client:$QUO_HOME
    sudo scp ips $client:$QUO_HOME
    sudo scp env.sh $client:$QUO_HOME
    echo $client index $i
#  ssh -o StrictHostKeyChecking=no $client 'cd /users/dinhtta/blockchain-perf/ethereum ; ./start-clients.sh '$3 $i $2
    sudo ssh -o StrictHostKeyChecking=no $client $QUO_HOME/start-clients.sh $3 $i $2 $4 $5 $6
    echo done with client $client
  fi
  let i=$i+1
#  echo $i
done
#echo "AT multi client 1"
if [[ $7 == "-drop" ]]; then
  let M=$2*10+$5*$6
  let SR=$M-150
  sleep 250
  let idx=$2-4
  let i=0
  for server in `cat $HOSTS`; do
    if [[ $i -ge $idx ]]; then
      sudo ssh -o StrictHostKeyChecking=no $server killall -KILL geth peer java
      echo "Dropped "$server
    fi
    let i=$i+1
  done
  sleep $SR
  for client in `cat $CLIENTS`; do
    echo $client index $i
    sudo ssh -o StrictHostKeyChecking=no $client 'killall -KILL driver' 
    let i=$i+1
  done
else
  let M=$2*50+2*$5
  sleep $M
  i=0
  for client in `cat $CLIENTS`; do
   if [[ $i -lt $1 ]] ; then
    echo $client index $i
    sudo ssh -o StrictHostKeyChecking=no $client 'killall -KILL driver' 
   # let i=$i+1
   fi
   let i=$i+1
  done
fi
