#!/bin/bash
#nodes
cd `dirname ${BASH_SOURCE-$0}`
. env.sh
. env_main.sh

#rm -R $LOG_DIR
rm -rf addPeer.txt
rm -rf ips
./gather.sh $1
echo "AFTER GATHER" 
sleep 3
i=0
for host in `cat $HOSTS`; do
  if [[ $i -lt $1 ]]; then
    scp addPeer.txt $host:$ETH_HOME
    scp clients $host:$ETH_HOME
    scp hosts $host:$ETH_HOME
    scp ips $host:$ETH_HOME
    scp env.sh $host:$ETH_HOME
    ssh -p 22 -o StrictHostKeyChecking=no $host $ETH_HOME/start-mining.sh
    echo done node $host
  fi
  let i=$i+1
done
