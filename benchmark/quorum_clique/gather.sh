#!/bin/bash
#nnodes
cd `dirname ${BASH_SOURCE-$0}`
. env.sh
. env_main.sh 

i=0
for host in `cat $HOSTS`; do
  if [[ $i -lt $1 ]]; then
    ip=`ssh -o StrictHostKeyChecking=no $host /sbin/ifconfig eno1d1 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}' `
    echo $ip
    result1=`ssh -o StrictHostKeyChecking=no $host $QUO_HOME/enode.sh $ip 2>/dev/null | grep enode`
    result=${result1//127.0.0.1/$ip}
    echo $result
    echo "admin.addPeer($result)" >> addPeer.txt
    echo $ip >> ips
  fi
  let i=$i+1
  #echo $i
done
#for com in `cat $ETH_HOME/addPeer.txt`; do
#    ip=`ssh -o StrictHostKeyChecking=no $host /sbin/ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}' `
#    #result=${com//127.0.0.1/$ip}
#    #sed '/^$com/d' addPeer.txt 
#    echo $ip >> ips
#done 

#mv addPeer2.txt addPeer.txt
