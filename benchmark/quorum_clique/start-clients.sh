#!/bin/bash
# args=THREADS index N txrate
echo IN START_CLIENTS $1 $2 $3 $4

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

#..
export CPATH=/usr/local/include
export LIBRARY_PATH=/usr/local/lib:$LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

#..

#LOG_DIR=$ETH_HOME/../src/ycsb/exp_$3"_"servers_run4
LOG_DIR=$LOG_DIR/exp_$3"_"servers_$1"_"threads_$4"_"rates
mkdir -p $LOG_DIR
i=0
IPS=$QUO_HOME/ips
for ip in `cat $IPS`; do
  let n=i/2
  let i=i+1
  cd $EXE_HOME
    #both ycsbc and smallbank use the same driver
    #nohup ./driver -db ethereum -ops 10000 -threads $1 -txrate  $4 -fp stat.txt -endpoint $host:8000 > $LOG_DIR/client_$host"_"$1 2>&1 &
#    ip=`/sbin/ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}' `
#    ip=`ssh -o StrictHostKeyChecking=no $host /sbin/ifconfig  ens1f1 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}' `
   nohup ./driver -db parity -ops 1000 -threads $1 -txrate  $4 -st $5 -fp stat.txt -endpoint $ip:8000 > $LOG_DIR/client_$ip"_"$1"_"$6 2>&1 &
done

