QUO_HOME=/users/tsalman/blockbench/benchmark/quorum_clique
HOSTS=$QUO_HOME/hosts
CLIENTS=$QUO_HOME/clients
QUO_DATA=/users/tsalman/quorum_clique
LOG_DIR=$QUO_HOME/clique_results_1
EXE_HOME=$QUO_HOME/../../src/macro/kvstore
BENCHMARK=ycsb
QUORUM=/users/tsalman/blockbench/benchmark/quorum_clique/quorum/build/bin/geth
ADDRESSES=$QUO_HOME/addresses

##comment these out for smallbank
EXE_HOME=$QUO_HOME/../../src/macro/smallbank
BENCHMARK=smallbank 
#LOG_DIR=$QUO_HOME/smallbank_results_2
