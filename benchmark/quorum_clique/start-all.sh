#!/bin/bash
#nodes
cd `dirname ${BASH_SOURCE-$0}`
. env.sh
. env_main.sh
echo "start-all.sh"
#rm -rf addPeer.txt
#./gather.sh $1
#sleep 3

i=0
for host in `cat $HOSTS`; do
  if [[ $i -lt $1 ]]; then
    echo $i
    scp clients $host:$QUO_HOME
#    scp addPeer.txt $host:$QUO_HOME
    scp hosts $host:$QUO_HOME
#    scp env.sh $host:$QUO_HOME
#    scp start-clients.sh $host:$QUO_HOME
    scp ips $host:$QUO_HOME
#    scp enode.sh $host:$QUO_HOME
#    scp start-mining.sh $host:$QUO_HOME
    ssh -p 22  -o StrictHostKeyChecking=no $host $QUO_HOME/start-mining.sh $1
    echo done node $host
  fi
  let i=$i+1
done
i=0
for host in `cat $HOSTS`; do
  if [[ $i -lt $1 ]]; then
    ssh -p 22 -oStrictHostKeyChecking=no  $host $QUO_HOME/start-addPeer.sh $i
    echo done node $host
  fi
  let i=$i+1
done
