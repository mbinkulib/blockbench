#!/bin/bash
#nodes
cd `dirname ${BASH_SOURCE-$0}`
. env.sh
. env_main.sh
echo "start-all.sh"
rm -rf addPeer.txt 
rm -rf ips 
./gather.sh $1
i=0
for host in `cat $HOSTS`; do
  if [[ $i -lt $1 ]]; then
    sudo scp clients $host:$QUO_HOME
#    scp addPeer.txt $host:$QUO_HOME
    sudo scp hosts $host:$QUO_HOME
#    scp env.sh $host:$QUO_HOME
#    scp start-clients.sh $host:$QUO_HOME
    sudo scp ips $host:$QUO_HOME
#    scp enode.sh $host:$QUO_HOME
#    scp start-mining.sh $host:$QUO_HOME
    sudo ssh -p 22  -o StrictHostKeyChecking=no $host $QUO_HOME/start-mining.sh $1 $2
    echo done node $host
  fi
  let i=$i+1
done
