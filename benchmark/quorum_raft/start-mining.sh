#!/bin/bash
cd `dirname ${BASH_SOURCE-$0}`
. env.sh
echo "start-mining.sh"

PRIVATE_CONFIG=ignore nohup ${QUORUM} --datadir $QUO_DATA --rpc --rpcaddr 0.0.0.0 --rpcport 8000 --port 9000 --raft --raftport 50400 --mine --rpcapi admin,db,eth,debug,miner,txpool,personal,web3,net,quorum,raft --allow-insecure-unlock --unlock 0 --password <(echo -n "") > $QUO_DATA/../raft_quorum_log_$2 2>&1 &
#echo --datadir $QUO_DATA --rpc --rpcaddr 0.0.0.0 --rpcport 8000 --port 9000 --raft --raftport 50400 --raftblocktime 2000 --unlock 0 --password <(echo -n "") 

#sleep 10
#echo "HERE"
#for com in `cat $QUO_HOME/addPeer.txt`; do
#  geth --exec $com attach ipc:/$QUO_DATA/geth.ipc
#done

