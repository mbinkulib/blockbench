#!/bin/bash
# num_nodes
cd `dirname ${BASH_SOURCE-$0}`
. env.sh
. env_main.sh

echo "init-all"
i=0
rm -rf ips
./gather.sh $1
./modify_ip.sh $1


for host in `cat $HOSTS`; do
  if [[ $i -lt $1 ]]; then
    sudo scp raft/static-nodes$1.json $host:$QUO_HOME/raft/static-nodes$1.json
    sudo scp genesis_quorum.json $host:$QUO_HOME/genesis_quorum.json
    sudo ssh -o StrictHostKeyChecking=no $host $QUO_HOME/init.sh $i $1
    echo done node $host
  fi
  let i=$i+1
done
