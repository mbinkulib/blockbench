#!/bin/bash
set -eu -o pipefail

#install npm
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get update
#sudo apt-get install -y npm
sudo apt-get install -y sshpass

# install golang
GOREL=go1.9.3.linux-amd64.tar.gz
wget -q https://dl.google.com/go/$GOREL
tar xfz $GOREL
if [ ! -e "/usr/local/go" ]; then
    sudo mv go /usr/local/go
fi
rm -f $GOREL
PATH=$PATH:/usr/local/go/bin
echo 'PATH=$PATH:/usr/local/go/bin' >> /users/mbk/.bashrc

# install restclient-cpp
sudo apt-get update
RESTAPI=restclient-cpp
sudo apt-get install -y dh-autoreconf
sudo apt-get install -y libcurl4-openssl-dev
if [ ! -e "/users/mbk/restclient-cpp" ]; then
   git clone https://github.com/mrtazz/restclient-cpp
fi 
cd $RESTAPI
./autogen.sh
./configure
sudo make install
cd /users/mbk
sudo chmod 700 /users/mbk
# get blockbench
if [ ! -e "/users/mbk/blockbench" ]; then
   git clone https://mbinkulib@bitbucket.org/mbinkulib/blockbench.git
fi
sudo chmod 777 -R blockbench
#mv blockbench /users/mbk

cd blockbench/src/micro
sudo npm install 
cd ../macro/smallbank
sudo make -B

cd /users/mbk/blockbench/benchmark/ethereum/
sudo chmod 400 HELLO.pem
sudo ./install.sh

cd ../quorum_raft
./install.sh
sudo ldconfig -v

cd ../quorum_clique
./install.sh
sudo ldconfig -v

#cd ../hyperledger
#sudo ./install.sh


#PARITY 
#cd /home/ubuntu
#git clone https://github.com/paritytech/parity-ethereum
#cd parity-ethereum
#curl https://sh.rustup.rs -sSf | sh -s -- -y
#source $HOME/.cargo/env
#sudo apt-get install -y cmake
#cargo build --release --features final



