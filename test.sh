#!/bin/bash
#arg nnodes
cd `dirname ${BASH_SOURCE-$0}`


echo "started" 
NODES=/home/blockchains/blockbench/NODES2
for host in `cat $NODES`; do
    echo "here" 
    sudo scp -o StrictHostKeyChecking=no bootstrap-awsNode.sh $host:/users/mbk
    sudo ssh -p 22 -o StrictHostKeyChecking=no $host /users/mbk/bootstrap-awsNode.sh
done 
