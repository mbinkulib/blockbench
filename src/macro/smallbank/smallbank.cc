
#include <cstring>
#include <string>
#include <iostream>
#include <vector>
#include <future>
#include <atomic>
#include <sstream>
#include <fstream>

#include "api_adapters/DB.h"
#include "api_adapters/SmallBank.h"
#include "api_adapters/EVMDB.h"
#include "utils/generators.h"
#include "utils/timer.h"
#include "utils/statistic.h"
#include "utils/utils.h"
#include "utils/properties.h"
//#define IVL 100

using namespace std;

atomic<unsigned long long> latency(0);
std::atomic<unsigned long long> latency_interval(0);
std::atomic<unsigned long long> ops(0);
std::ofstream os_;
Timer<double> stat_timer;

vector<double> effThr;
vector<string> allfintx;

const int HL_CONFIRM_BLOCK_LENGTH = 1;
const int BLOCK_POLLING_INTERVAL = 2; 
const int CONFIRM_BLOCK_LENGTH = 5;
const int PARITY_CONFIRM_BLOCK_LENGTH = 1;
int  totaltx; 
long block_txdelay;
long effective_delay;
bool blockstart;
bool minerstart;

//int tempInt;
int notCalculated;

void UsageMessage(const char *command);
bool StrStartWith(const char *str, const char *pre);
string ParseCommandLine(int argc, const char *argv[], utils::Properties &props);

SpinLock spinlock_, txlock_;
std::unordered_map<string, double> pendingtx;
std::unordered_map<string, bool> pendingtxchecked;
std::unordered_map<string, double> finalizeddelay;
std::unordered_map<string, double> commitdelay;


int noOperation;

void ClientThread(DB* sb, const int num_ops, const int txrate) 
{
  noOperation = num_ops;
  UniformGenerator op_gen(1, 6);
  UniformGenerator acc_gen(1, 100000);
  UniformGenerator bal_gen(1, 10);
  double tx_sleep_time = 1.0 / txrate;
  for (int i = 0; i < num_ops; ++i) 
  {
    auto op = op_gen.Next();
    //cout << i << endl; //step1: to ensure the the right number of transactions are sent
    //cout << "op: " << op << endl; // step3: to check if specific op not added to the pendingtx
    switch (op) {
      //cout << "op: " << op << endl; // step3: to check if specific op not added to the pendingtx
      case 1:
        sb->Amalgate(acc_gen.Next(), acc_gen.Next());
        break;
      case 2:
        sb->GetBalance(acc_gen.Next());
        break;
      case 3:
        sb->UpdateBalance(acc_gen.Next(), 0);
        break;
      case 4:
        sb->UpdateSaving(acc_gen.Next(), 0);
        break;
      case 5:
        sb->SendPayment(acc_gen.Next(), acc_gen.Next(), 0);
        break;
      case 6:
        sb->WriteCheck(acc_gen.Next(), 0);
        break;
    }
    utils::sleep(tx_sleep_time*2);
    //cout << "i= " << i << endl;
    //cout << "pendingtx.size(): " << pendingtx.size() << endl; // step2: to ensure that each transmitted tx is added to the pendingtx
  }
  
}


int StatusThread(DB* sb, string dbname, string endpoint, double interval, int start_block_height, int st)
{
    int countForThr = 0;
    int cur_block_height = start_block_height;
    long start_time;
    long end_time;
    int txcount = 0;
    long latency;
    int confirm_duration = 1;
    blockstart = false;
    minerstart=false ;
    long timerstart ;
    long timerfin;
  
    
    if (dbname == "ethereum") confirm_duration = CONFIRM_BLOCK_LENGTH;
    else if (dbname == "parity") confirm_duration = PARITY_CONFIRM_BLOCK_LENGTH;
    else confirm_duration = HL_CONFIRM_BLOCK_LENGTH;
    
    
    timerstart= time_now();
    //tempInt = 0;
    while(true)
    {
        start_time = time_now();
        int tip = sb->get_tip_block_number();
        if (tip==-1) // fail
        sleep(interval);
        
        //calculate finalized latency for finalized transactions and remove these transactions from pending/outstanding transactions list
        while (cur_block_height + confirm_duration <= tip+1) 
		    {
            
			    vector<string> txs = sb->poll_tx(cur_block_height);
          cout << "polled block " << cur_block_height << " : " << txs.size() << " txs " << endl;
			    countForThr+=txs.size();
          totaltx+=txs.size();
          cur_block_height++;  
          txlock_.lock();
          
          //remove all finalized transactions after calculating its finalized delay
          for (string tmp : txs)
			    {
            //tempInt++;
            //cout << "tempInt: " << tempInt << endl;

            string s = (dbname == "ethereum" || dbname == "parity") ? tmp.substr(1, tmp.length() - 2) : tmp;
            allfintx.push_back(s);
            cout << "Transaction info: "  << tmp << endl;
            long block_time = time_now();
                if (pendingtx.find(s)!=pendingtx.end())
                {
                  txcount++;
                  finalizeddelay[s]=(block_time - pendingtx[s])/1000000000.0 ;
                  latency += (block_time - pendingtx[s]);
                  pendingtx.erase(s);
                }
                else cout << "transaction not found " << endl;
            }
            
            txlock_.unlock();
        }



        if (txcount!=0 && !blockstart) 
        {
          block_txdelay=time_now();
          blockstart= true; 
        }

        if (txcount==0 && blockstart) 
        {
          block_txdelay=time_now()-block_txdelay;
          blockstart= false; 
        }
        
        if (pendingtx.size()!=0 && !minerstart) 
        {
          effective_delay= time_now();
          minerstart= true;  
        }

        if(pendingtx.size()==0 && minerstart) 
        {
          effective_delay= time_now()-effective_delay;
          minerstart= false; 
        }

        //Modhi's Code
        cout << "Total number of transactions:  "<< countForThr << endl;
        if (countForThr != 0)  
          effThr.push_back(countForThr);		
        countForThr=0;
		
        cout << "In the last "<< interval <<"s, tx count = " << txcount
        << " latency = " << latency/1000000000.0
        << " outstanding request = " << pendingtx.size() << endl;
        //cout << "finalizeddelay.size(): "<< finalizeddelay.size() << endl;
        txcount = 0;
        latency = 0;
        
        end_time = time_now();
        timerfin= time_now();

        //Loop break condition
        if (((timerfin- timerstart)/ 1000000000.0) > st)
        {
          //if (finalizeddelay.size() >= 1000)
          {
            //cout << "Simulation time  "<< ((timerfin- timerstart)/ 1000000000.0) << endl;
            //notCalculated = 4000-pendingtx.size();
            break;
          }
        } 
        
        utils::sleep(interval - (end_time - start_time) / 1000000000.0);
    }
    return 0;
}




int CheckPendingTransactions(DB* sb, string dbname, string endpoint, double interval, int start_block_height, int st)
{
    //int countForThr = 0;
    int cur_block_height = start_block_height;
    //long start_time;
    //long end_time;
    //int txcount = 0; //counting finalized transactions each 2 seconds 
    //long latency;
    int confirm_duration = 1;
    //blockstart = false;
    //minerstart=false ;
    //long timerstart ;
    //long timerfin;
    int total=0;
    
    if (dbname == "ethereum") confirm_duration = CONFIRM_BLOCK_LENGTH;
    else if (dbname == "parity") confirm_duration = PARITY_CONFIRM_BLOCK_LENGTH;
    else confirm_duration = HL_CONFIRM_BLOCK_LENGTH;
    
    
    //timerstart= time_now();
    

   // while(true)
    //{
        //start_time = time_now();
        int tip = sb->get_tip_block_number();
        if (tip==-1) // fail
        sleep(interval);
        
        //int tempInt = 0;
        //calculate finalized latency for finalized transactions for each block and remove these transactions from pending/outstanding transactions list
        while (cur_block_height + confirm_duration <= tip) 
		    {
          //extract transaction lists from the confirmed block's content  
			    vector<string> txs = sb->poll_tx(cur_block_height);
          cout << "polled block " << cur_block_height << " : " << txs.size() << " txs " << endl;
			    //countForThr+=txs.size();
         total += txs.size();
          //totaltx+=txs.size(); // increment totaltx from each finalized block, meaning that this is the total number of finalized transactions for all hosts
          cur_block_height++;  
          txlock_.lock();
          
          //remove all finalized transactions after calculating its finalized delay
          //all transactions go through this loop
          for (string tmp : txs)
			    {
            //tempInt++:
            //cout << "tempInt: " << tempInt << endl;
            string s = (dbname == "ethereum" || dbname == "parity") ? tmp.substr(1, tmp.length() - 2) : tmp;
            long block_time = time_now();
                //remove finalized transactions from the local queue
                if (pendingtx.find(s)!=pendingtx.end())
                {
                  cout << "A TRANSACTION FOUND *************** " << endl;
                  //txcount++;
                  //finalizeddelay[s]=(block_time - pendingtx[s])/1000000000.0 ;
                  //latency += (block_time - pendingtx[s]);
                  pendingtx.erase(s);
                }
                //else cout << "transaction not found " << endl;
            }
            
            txlock_.unlock();
       }


/*
        if (txcount!=0 && !blockstart) 
        {
          block_txdelay=time_now();
          blockstart= true; 
        }

        if (txcount==0 && blockstart) 
        {
          block_txdelay=time_now()-block_txdelay;
          blockstart= false; 
        }
        
        if (pendingtx.size()!=0 && !minerstart) 
        {
          effective_delay= time_now();
          minerstart= true;  
        }

        if(pendingtx.size()==0 && minerstart) 
        {
          effective_delay= time_now()-effective_delay;
          minerstart= false; 
        }

        //Modhi's Code
        cout << "Total number of transactions:  "<< countForThr/2 << endl;
        if (countForThr != 0)  
          effThr.push_back(countForThr/2);		
        countForThr=0;
	*/	
        //cout 
        //<< "In the last "<< interval <<"s, tx count = " << txcount
        //<< " latency = " << latency/1000000000.0
        //<< " outstanding request = " << pendingtx.size() << endl;
        //txcount = 0;
        //latency = 0;
        
        //end_time = time_now();
        //timerfin= time_now();

        //Loop break condition
        /*if (((timerfin- timerstart)/ 1000000000.0) > st)
        {
          if (finalizeddelay.size() >= 1000)
          {
            cout << "Simulation time  "<< ((timerfin- timerstart)/ 1000000000.0) << endl;
            break;
          }
        } */
        
        //utils::sleep(interval - (end_time - start_time) / 1000000000.0);
   // }
   cout << "total: " << total << endl;
    return 0;
}



int StatusThread2(DB* sb, string dbname, string endpoint, double interval, int start_block_height, int st)
{
    
    double countForThr = 0; //Modhi
    int cur_block_height = start_block_height;
    long start_time;
    long end_time;
    int txcount = 0;
    long latency;
    blockstart = false;
    minerstart=false ;
    long timerstart ;
    long timerfin;
    timerstart= time_now();
    while(true){
        start_time = time_now();
        int tip = sb->get_tip_block_number();
        if (tip==-1) // fail
        sleep(interval);
        while (cur_block_height <= tip) {
            vector<string> txs = sb->poll_tx(cur_block_height);
            cout << "polled block " << cur_block_height << " : " << txs.size()
            << " txs " << endl;
            countForThr+=txs.size(); //Modhi
            totaltx+=txs.size(); //Modhi

            cur_block_height++;
            txlock_.lock();
            for (string tmp : txs){
                string s = (dbname == "ethereum" || dbname == "parity")
                ? tmp.substr(1, tmp.length() - 2)  // get rid of ""
                : tmp;
                long block_time = time_now();
                if (pendingtx.find(s)!=pendingtx.end()){
                    txcount++;
                    commitdelay[s]=(block_time - pendingtx[s])/1000000000.0 ;
		    //pendingtx.erase(s);//not executing this becuase technically we don't know if it is finalized
                }
                //else cout << "transaction not found, this is a contract that is submitted from the client but its latency is not calculated and it will not have significant impact in throughput calculations " << endl;
            }
            txlock_.unlock();
        }
       //Modhi's Code
        //cout << "Total number of transactions:  "<< countForThr << endl;
        if (countForThr != 0)  
          effThr.push_back(countForThr/2);
        countForThr=0;
        
        cout << "In the last "<< interval <<"s, tx count = " << txcount
      //  << " latency = " << latency/1000000000.0
        << " outstanding request = " << pendingtx.size() << endl;
        txcount=0;

        end_time = time_now();
        timerfin= time_now() ;
        //if ((((timerfin- timerstart)/ 1000000000.0) > st) && (pendingtx.size() == noOperation))
        if (((timerfin- timerstart)/ 1000000000.0) > st) 
        break ;
        //sleep in nanosecond
        /*else if ((((timerfin- timerstart)/ 1000000000.0) > st) && (pendingtx.size() < noOperation)){
        st = st + 100;
        cout << "new st:" << st << endl; }*/
        utils::sleep(interval - (end_time - start_time) / 1000000000.0);
    }
    return 0;
}

DB* CreateDB(std::string dbname, std::string endpoint) 
{
  if (dbname == "hyperledger") return SmallBank::GetInstance("SmallbankExample", endpoint);
  else if (dbname == "ethereum" || dbname == "parity") return EVMDB::GetInstance(dbname, endpoint);
  else return NULL;
}

int main(const int argc, const char* argv[]) {
  utils::Properties props;
  string filename = ParseCommandLine(argc, argv, props);

  DB* sb = CreateDB(props["dbname"], props["endpoint"]);
  if (!sb) {
    cout << "Unknown database name " << props["dbname"] << endl;
    exit(0);  
  }
  if (os_.is_open()) {
    os_.close();
  }
  os_.open(props.GetProperty("file_path", "stat.txt"), std::ios::app);

  int current_tip = sb->get_tip_block_number();
  cout << "Current TIP = " << current_tip << endl;

 
  sb->Init( &pendingtx,&pendingtxchecked, &txlock_);
  const int thread_num = stoi(props.GetProperty("threadcount", "1"));
  const int txrate = stoi(props.GetProperty("txrate", "10"));
  const int total_ops = stoi(props.GetProperty("total_ops", "10000")); 
  cout <<"total_ops= " << total_ops << endl ;
  const int st = stoi(props.GetProperty("st", "300"));
  vector<thread> threads;
  Timer<double> timer;
  timer.Start();
  stat_timer.Tic();
  cout << "pending transactions: " << pendingtx.size() << endl;
  for (int i = 0; i < thread_num; ++i) 
  {
    //cout <<"int i = 0; i < thread_num; ++i, i=" << i << endl ;
    //cout <<"total_ops / thread_num sent to ClientThread" << total_ops << endl ;
    threads.emplace_back(ClientThread, sb, total_ops / thread_num, txrate);
    //ClientThread(sb, total_ops / thread_num, txrate);
  }
  threads.emplace_back(StatusThread2, sb, props["dbname"], props["endpoint"], BLOCK_POLLING_INTERVAL, current_tip, st);
  //StatusThread2(sb, props["dbname"], props["endpoint"], BLOCK_POLLING_INTERVAL, current_tip, st);
  //threads.emplace_back(StatusThread, sb, props["dbname"], props["endpoint"], BLOCK_POLLING_INTERVAL, current_tip, st);
  //threads.emplace_back(StatusThread2, sb, props["dbname"], props["endpoint"], BLOCK_POLLING_INTERVAL, current_tip, st);
  //StatusThread(sb, props["dbname"], props["endpoint"], BLOCK_POLLING_INTERVAL, current_tip, st);

  
  for (auto& th : threads) 
    th.join();
  
  double duration = timer.End();
  double avrc_delay= 0;
  double avrf_delay= 0;
  int index=0 ;
  int index2=0 ;
  
  cout <<endl <<endl<< "finalized delay"<< endl;
  for (auto const& pair: finalizeddelay)
  {
       if (index <= finalizeddelay.size()) 
       {
          cout <<pair.second<<endl;
          avrf_delay+=pair.second ;
          index=index+1;
       }
       else
        break ;
   }
   
   
   
   cout<<endl<<endl<<"Commit delays"<<endl ;
   for (auto const& pair: commitdelay)
   {
        if (index2 <= commitdelay.size()) {
            cout <<pair.second<<endl;
            avrc_delay+=pair.second ;
            index2=index2+1;
        }
        else { break ; }
   }


   cout << endl << endl ;
   if (blockstart)
    {
         block_txdelay=time_now()-block_txdelay ;
    }
   
   if (minerstart)
   {
         effective_delay= time_now()-effective_delay ;
    }
   
   //Modhi's code for effective throughout calculation
   double sum = 0;
   cout << "Effective Throughput:" << endl; 
   cout << endl;
   for (int i =0; i < effThr.size(); i++) 
   { 
     cout << effThr[i] << endl;
     sum+=effThr[i];  
   }
  double av = sum/effThr.size();
   //if (tempInt == totaltx)
   //{
   //cout << "ALL SUBMITTED TRANSACTIONS ARE FINALIZED" << endl ;
   //if (notCalculated != 0) cout << "notCalculated: " << notCalculated << endl ; 
   //}

//re-check for outstanding transactions
/*  cout <<"Number of finalized transactions= " << finalizeddelay.size() << endl ;
  cout <<"Total number of operations= " << total_ops << endl ;
  if (finalizeddelay.size() != total_ops)
  {
        CheckPendingTransactions(sb, props["dbname"], props["endpoint"], BLOCK_POLLING_INTERVAL, current_tip, st);
for (auto const& pair: pendingtx) 
        {
		      std::cout << "{" << pair.first << ": " << pair.second << "}\n";
        }
  }
   cout << "number of allfintx: " << allfintx.size() << endl;
sort( allfintx.begin(), allfintx.end() );
allfintx.erase( unique( allfintx.begin(), allfintx.end() ), allfintx.end() );
cout << "number of allfintx after removing duplicates: " << allfintx.size() << endl;
*/
   cout <<"Number of finalized transactions= " << finalizeddelay.size() << endl ;
   cout <<"Number of commited transactions= " << commitdelay.size() << endl ;
   cout <<"Average commit delay= " << avrc_delay/commitdelay.size() <<"seconds" <<endl ;
   cout <<"Average finzalized delay= " << avrf_delay/finalizeddelay.size() <<"seconds" <<endl ;
   cout << totaltx << "      "<<block_txdelay/1000000000.0 << endl ; 
  cout <<"effective throughout= "<< av  <<" tps"<<endl;
  cout << "Overall transaction throughput=";
  cout << totaltx / duration<< " tps" <<endl;


  if (os_.is_open()) os_.close();
  return 0;
}

string ParseCommandLine(int argc, const char *argv[], utils::Properties &props) 
{
  int argindex = 1;
  string filename;
  while (argindex < argc && StrStartWith(argv[argindex], "-")) {
    if (strcmp(argv[argindex], "-threads") == 0) {
      argindex++;
      if (argindex >= argc) {
        UsageMessage(argv[0]);
        exit(0);
      }
      props.SetProperty("threadcount", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-db") == 0) {
      argindex++;
      if (argindex >= argc) {
        UsageMessage(argv[0]);
        exit(0);
      }
      props.SetProperty("dbname", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-endpoint") == 0) {
      argindex++;
      if (argindex >= argc) {
        UsageMessage(argv[0]);
        exit(0);
      }
      props.SetProperty("endpoint", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-txrate") == 0) {
      argindex++;
      if (argindex >= argc) {
        UsageMessage(argv[0]);
        exit(0);
      }
      props.SetProperty("txrate", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-wl") == 0) {
      argindex++;
      if (argindex >= argc) {
        UsageMessage(argv[0]);
        exit(0);
      }
      props.SetProperty("workload", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-wt") == 0) {
      argindex++;
      if (argindex >= argc) {
        UsageMessage(argv[0]);
        exit(0);
      }
      props.SetProperty("deploy_wait", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-ops") == 0) {
      argindex++;
      if (argindex >= argc) {
        UsageMessage(argv[0]);
        exit(0);
      }
      props.SetProperty("total_ops", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-st") == 0) {
      argindex++;
      if (argindex >= argc) {
        UsageMessage(argv[0]);
        exit(0);
      }
      props.SetProperty("st", argv[argindex]);
      argindex++;
     } else if (strcmp(argv[argindex], "-fp") == 0) {
      argindex++;
      if (argindex >= argc) {
        UsageMessage(argv[0]);
        exit(0);
      }
      props.SetProperty("file_path", argv[argindex]);
      argindex++;

    } else if (strcmp(argv[argindex], "-P") == 0) {
      argindex++;
      if (argindex >= argc) {
        UsageMessage(argv[0]);
        exit(0);
      }
      filename.assign(argv[argindex]);
      ifstream input(argv[argindex]);
      try {
        props.Load(input);
      } catch (const string &message) {
        cout << message << endl;
        exit(0);
      }
      input.close();
      argindex++;
    } else {
      cout << "Unknown option '" << argv[argindex] << "'" << endl;
      exit(0);
    }
  }

  if (argindex == 1 || argindex != argc) {
    UsageMessage(argv[0]);
    exit(0);
  }

  return filename;
}

void UsageMessage(const char *command) {
  cout << "Usage: " << command << " [options]" << endl;
  cout << "Options:" << endl;
  cout << "  -threads n: execute using n threads (default: 1)" << endl;
  cout << "  -wt deploytime: waiting time in second before start to submit "
          "transactions for deployment the smart "
          "contract/chaincode" << endl;
  cout << "  -db dbname: specify the name of the DB to use (e.g., hyperledger)"
       << endl;
  cout << "  -P propertyfile: load properties from the given file. Multiple "
          "files can" << endl;
  cout << "                   be specified, and will be processed in the order "
          "specified" << endl;
  cout << " -st specifies the simulation time in seconds. Default is 300 (5 miniutes" <<endl ; 
  cerr << "   eg: " << "./driver"
         << " -ops 10000 -threads 4 -txrate 10 -fp stat.txt -endpoint 127.0.0.1:8000 -db ethereum -st 60" << endl;
}

inline bool StrStartWith(const char *str, const char *pre) {
  return strncmp(str, pre, strlen(pre)) == 0;
}
